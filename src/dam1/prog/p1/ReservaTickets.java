package dam1.prog.p1;
/*
 * Se pretende realizar un programa que sirva para simular un tren con tres vagones. En cada vagón habrá 16 filas y 4
 * columnas. Cuando se inicie el programa la estrutura deberá de tener todas las posiciones de sus asientos libres. Se
 * debe preguntar al usuario que posición desea reservar y en caso de estar libre asignarla como ocupada y ya no se
 * podrá ocupar por ningún cliente más, avisando, en caso de ser solicitada de nuevo, que la plaza ya está ocupada y
 * pidiendo al usuario que introduzca una plaza libre.
 *
 * La estructura se rellenará con caracteres 'L' para libre y 'O' para ocupado en el caso de que esa posición sea
 * reservada.
 *
 * También se le preguntará al usuario el número de vagón que desea. Sería interesante dal la información de plazas
 * libres al usuario antes de que realice su elección.
 *
 * Se tendrá en cuenta la posición de reserva automática si el usuario simplemente quiere que se le asigne la primera
 * plaza libre que se enceuntre en el tren.
 */

import java.util.Scanner;

/**
 * @author Beatriz
 */
public class ReservaTickets {
    public static void main(String[] args) {
        //Creamos el objeto scanner//
        Scanner key = new Scanner(System.in);

        //Iniciamos las matrices//
        char[][] vagon1 = new char[16][4];
        char[][] vagon2 = new char[16][4];
        char[][] vagon3 = new char[16][4];

        //Rellenamos los vagones con los asientos libres//
        for (int i = 0; i < vagon1.length; i++) {
            for (int j = 0; j < vagon1[i].length; j++) {
                vagon1[i][j] = 'L';
            }
        }

        for (int i = 0; i < vagon2.length; i++) {
            for (int j = 0; j < vagon2[i].length; j++) {
                vagon2[i][j] = 'L';
            }
        }

        for (int i = 0; i < vagon3.length; i++) {
            for (int j = 0; j < vagon3[i].length; j++) {
                vagon3[i][j] = 'L';
            }
        }

        int intentos = 0;
        do {
            //Le preguntamos al usuario si quiere elegir su asiento o si desea que se le asigne automaticamente//
            System.out.println("¿Desea elegir su aisento o que se le asigne uno automaticamente?(elegir asiento - 1, automaticamente - 2)");
            int eleccion = key.nextInt();

            if (eleccion == 1) {
                boolean asientoOcupado;
                //Preguntamos al usuario por el vagón que quiere//
                do {
                    System.out.println("¿En qué vagón desea hacer la reserva?");
                    //Le mostramos los asientos que están vacios u ocupados para que elija//
                    System.out.println("Vagon 1");

                    for (int i = 0; i < vagon1.length; i++) {
                        System.out.println();
                        for (int j = 0; j < vagon1[i].length; j++) {
                            System.out.print(vagon1[i][j] + " ");
                        }
                    }

                    System.out.println();
                    System.out.println();
                    System.out.println("Vagon 2");
                    for (int i = 0; i < vagon2.length; i++) {
                        System.out.println();
                        for (int j = 0; j < vagon2[i].length; j++) {
                            System.out.print(vagon2[i][j] + " ");
                        }
                    }

                    System.out.println();
                    System.out.println();
                    System.out.println("Vagon 3");
                    for (int i = 0; i < vagon3.length; i++) {
                        System.out.println();
                        for (int j = 0; j < vagon3[i].length; j++) {
                            System.out.print(vagon3[i][j] + " ");
                        }
                    }
                    System.out.println();

                    int vagon = key.nextInt();

                    //Le pedimos la fila y la columna para el asiento que quiere reservar//
                    System.out.println("Indique la fila en la que desea sentarse");
                    int fila = key.nextInt() - 1;
                    System.out.println("Indique la columna en la que desea sentarse");
                    int columna = key.nextInt() - 1;

                    asientoOcupado = true;

                    //Comprobamos que el asiento elegido no este ocupado en el vagón y si esta libre lo asignamos//
                    if (vagon == 1) {
                        if (vagon1[fila][columna] == 'O') {
                            System.out.println("El asiento esta ocupado");
                        } else {
                            vagon1[fila][columna] = 'O';
                            asientoOcupado = false;
                        }
                    } else if (vagon == 2) {
                        if (vagon2[fila][columna] == 'O') {
                            System.out.println("El asiento esta ocupado");
                        } else {
                            vagon2[fila][columna] = 'O';
                            asientoOcupado = false;
                        }
                    } else if (vagon == 3) {
                        if (vagon3[fila][columna] == 'O') {
                            System.out.println("El asiento esta ocupado");
                        } else {
                            vagon3[fila][columna] = 'O';
                            asientoOcupado = false;
                        }
                    }
                } while (asientoOcupado);

            } else {

                //Le generamos el asiento de forma automática en el primer asiento que este libre//
                for (int i = 0; i < 16; i++) {
                    for (int j = 0; j < 4; j++) {
                        if (vagon1[i][j] == 'L') {
                            vagon1[i][j] = 'O';
                            break;
                        } else if (vagon2[i][j] == 'L') {
                            vagon2[i][j] = 'O';
                            break;
                        } else if (vagon3[i][j] == 'L') {
                            vagon3[i][j] = 'O';
                            break;
                        }
                    }
                    break;
                }
            }

            //Mostramos el asiento elegido ocupado como que la reserva esta realizada correctamente//
            System.out.println("Vagon 1");
            for (int i = 0; i < vagon1.length; i++) {
                System.out.println();
                for (int j = 0; j < vagon1[i].length; j++) {
                    System.out.print(vagon1[i][j] + " ");
                }
            }

            System.out.println();
            System.out.println();
            System.out.println("Vagon 2");
            for (int i = 0; i < vagon2.length; i++) {
                System.out.println();
                for (int j = 0; j < vagon2[i].length; j++) {
                    System.out.print(vagon2[i][j] + " ");
                }
            }

            System.out.println();
            System.out.println();
            System.out.println("Vagon 3");
            for (int i = 0; i < vagon3.length; i++) {
                System.out.println();
                for (int j = 0; j < vagon3[i].length; j++) {
                    System.out.print(vagon3[i][j] + " ");
                }
            }

            System.out.println();
            intentos++;
        } while (intentos <= 3);
    }
}
